all: pmc.bib try.tex
	rm try.bbl
	latex try && bibtex try && latex try && (clear; echo '--- last run stars here!!!'; latex try) && ( echo ; echo "OK, you may commit" )
push:
	git commit -a
	git push
	echo "No new files added! Committed only changes to already tracked files."
clean:
	rm -f try.aux try.bbl try.bcf try.blg try-blx.bib try.dvi try.log try.run.xml
